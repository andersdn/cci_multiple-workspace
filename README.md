# Circle CI Examples #

## Using multiple persisted workspaces in a single pipeline ##

This repo is to show example congig of a circleCI pipeline with multiple instances of `persist_to_workspace` to allow two parallel jobs to contribue artifacts to a subsequent job.

## How do we do it?

By using *unique* `persist_to_workspace` blocks (in this instance having a custom temp folder) you can re-use persistance blocks, otherwise these will be overwritten in a "last one wins" scenario.

```
run-job-a:
    steps:
        # ...etc
        - persist_to_workspace:
            root: /tmp/workspace-a
            paths:
                - joba.txt # just a single file for this example
...
run-job-b:
    steps:
        # ...etc
        - persist_to_workspace:
            root: /tmp/workspace-b
            paths:
                - jobb.txt # just a single file for this example
```            

This can then be picked up by a `attach_workspace` block (noting that the resulting folder is flat) which will mount at the given path (in this instance `/tmp/workspace`)
```
read-job-results:
    steps:
        - attach_workspace:
            at: /tmp/workspace
```

> The important thing is that the _same_ folder is not re-used, so either use unique sub folders, or if the end result is going to be in a common folder, copy to unique temp folders


## What are the results like?

### Workflow:
![workflow](./workflow.png)

### Results:
![workflow](./results.png)
